/**
 * Created by Lucas on 20/05/2016.
 */

//Desafio: Crawler para Twitter
//
//Neste desafio, você deverá implementar um código JavaScript que extraia os tweets de um determinado perfil do Twitter. Caso exista algum link no tweet, o mesmo deverá ser removido.
//
//    Após obter os tweets do perfil, você deverá exibi-los na própria página, por meio da função document.write.
//
//    Para fim de testes, utilize o perfil da UFMG.


var crawler = function(){
    var twitter = document.body.getElementsByClassName("tweet-text");
    document.write("<!DOCTYPE html><html><head><title>Twitter</title></head><body></body></html>");

    for(var i = 0; i < twitter.length;++i){
        document.write("<h1>Tweet</h1><p>" +twitter[i].textContent + "</p>");
    }
}

crawler();
