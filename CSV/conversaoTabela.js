
var geraTabela = function(t){
    
    
    var div = t.parentNode;
    
    var tabelacsv = div.querySelectorAll("p");
    
    var nColunas = contaColunas(tabelacsv[0].innerHTML) + 1;
    
    var nLinhas = tabelacsv.length;
    
    var tabela = document.createElement("table");
    
    for(var i = 0; i < nLinhas; ++i){
        
        
        var linha = document.createElement("tr");
        
        tabela.appendChild(linha)
        
        for(var j = 0; j < nColunas; ++j){
            if(i == 0){
                var coluna = document.createElement("th");
                coluna.innerHTML = getcabecalho(j + 1,tabelacsv[0].innerHTML,nColunas);
                linha.appendChild(coluna);
            }
            else{       
                var coluna = document.createElement("td");
                console.log(tabelacsv[j].innerHTML);
                coluna.innerHTML = getcabecalho(j + 1,tabelacsv[i].innerHTML,nColunas);
                linha.appendChild(coluna);
            }   
        }
    }
    
    
    div.appendChild(tabela);
    

    t.removeAttribute("onclick");
}


var getcabecalho = function(col,linha,ncol){
    
    
    var count = 0;
    var anterior = 0;
    var atual = 0;
    

    if(col == ncol){
        return linha.substring(linha.lastIndexOf(',') + 1,linha.length);
    }
        
    
    for(var i = 0; i < linha.length;++i){
        if(linha.charAt(i) == ','){
            
            anterior = atual;
            atual = i;
            count++;
            //console.log(anterior + " " + atual);
        }
        
        if(count == col){
            if(count != 1){
                return linha.substring(anterior + 1,atual);    
            }
            return linha.substring(anterior,atual);
        }
        
            
    }
}

var contaColunas = function(head){
    
    var numeroColuas = 0;
    
    for(var i = 0; i < head.length; ++i){
        if(head.charAt(i) == ',')
            numeroColuas++;
    }

    return numeroColuas;
}

